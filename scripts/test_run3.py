import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.TAU.tauCorrections import tauSFRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def tau_sf_test(df, isMC, year, runPeriod):
    tausf = tauSFRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        vsjet_wps = ["Medium"],
        vse_wps = ["VVLoose"],
        vsmu_wps = ["VLoose"],
        vsjet_vse_wp = "VVLoose",
    )()
    df, _ = tausf.run(df)
    h = df.Histo1D("tes_factor_Medium")
    print(f"Tau {year}{runPeriod} tes_factor SF Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df


if __name__ == "__main__":
    df_mc2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022.root")))
    _ = tau_sf_test(df_mc2022, True, 2022, "preEE")

    df_mc2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022_postee.root")))
    _ = tau_sf_test(df_mc2022_postEE, True, 2022, "postEE")
