import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.TAU.tauCorrections import tauSFRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def tau_sf_test(df, isMC, year, isUL, runPeriod):
    tausf = tauSFRDF(
        isMC=isMC,
        year=year,
        isUL=isUL,
        runPeriod=runPeriod,
        vsjet_wps = ["Medium"],
        vse_wps = ["VVLoose"],
        vsmu_wps = ["VLoose"],
        vsjet_vse_wp = "VVLoose",
    )()
    df, _ = tausf.run(df)
    h = df.Histo1D("tes_factor")
    print(f"Tau {year}{runPeriod} tes_factor SF Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

if __name__ == "__main__":
    df_mc2016preVFP = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2016.root")))
    _ = tau_sf_test(df_mc2016preVFP, True, 2016, True, "preVFP")

    df_mc2016postVFP = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2016_apv.root")))
    _ = tau_sf_test(df_mc2016postVFP, True, 2016, True, "postVFP")

    df_mc2017 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2017.root")))
    _ = tau_sf_test(df_mc2017, True, 2017, True, "")

    df_mc2018 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2018.root")))
    _ = tau_sf_test(df_mc2018, True, 2018, True, "")
