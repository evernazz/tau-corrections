import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()


   
#          ╭────────────────────────────────────────── ▶ input ───────────────────────────────────────────╮
# │   │ syst (string)                                                                                │
# │   │ Systematic variations for the pT-binned SF: 'up'/'down' (the total uncertainty) ,            │
# │   │ 'stat_ptbin1_up'/'stat_ptbin1_down' (stat. uncertainty for 20<pt<25 bin) ,                   │
# │   │ 'stat_ptbin2_up'/'stat_ptbin2_down' (stat. uncertainty for 25<pt<30 bin) ,                   │
# │   │ 'stat_ptbin3_up'/'stat_ptbin3_down' (stat. uncertainty for 30<pt<35 bin) ,                   │
# │   │ 'stat_ptbin4_up'/'stat_ptbin4_down' (stat. uncertainty for 35<pt<40 bin) ,                   │
# │   │ 'stat_ptbin5_up'/'stat_ptbin5_down' (stat. uncertainty for 40<pt<50 bin) ,                   │
# │   │ 'stat_ptbin6_up'/'stat_ptbin6_down' (stat. uncertainty for 50<pt<60 bin) ,                   │
# │   │ 'stat_ptbin7_up'/'stat_ptbin7_down' (stat. uncertainty for 60<pt<80 bin) ,                   │
# │   │ 'stat_ptbin8_up'/'stat_ptbin8_down' (stat. uncertainty for 80<pt<100 bin) ,                  │
# │   │ 'stat_ptbin9_up'/'stat_ptbin9_down' (stat. uncertainty for 100<pt<140 bin) ,                 │
# │   │ 'syst_alleras_up'/'syst_alleras_down' (syst. uncertainty for low pT bins correlated by eras) │
# │   │ , 'syst_$ERA_up'/'syst_$ERA_down' (syst. uncertainty for low pT bins uncorrelated by eras    │
# │   │ $ERA=2016_preVFP,2016_postVFP,2017,2018) , 'stat_highpT_bin1_up'/'stat_highpT_bin1_down'     │
# │   │ (stat. uncertainty for 140<pt<200 bin) , 'stat_highpT_bin2_up'/'stat_highpT_bin2_down'       │
# │   │ (stat. uncertainty for pt>200 bin) , 'syst_highpT_up'/'syst_highpT_down' (syst. uncertainty  │
# │   │ for high pT bins correlated by eras) , 'syst_highpT_extrap_up'/'syst_highpT_extrap_down'     │
# │   │ (syst. uncertainty to account for the extrapolation of SF from measured pT regions to higher │
# │   │ pT regions correlated by eras) , Systematic variations for the DM-binned SF:                 │
# │   │ 'stat$i_dm$DM_up'/'stat$i_dm$DM_up' (stat. uncertainty for dm bin from ith eigenvector       │
# │   │ $i=1,2 $DM=0,1,10,11) , 'syst_alleras_up'/'syst_alleras_down' (syst. uncertainty for low pT  │
# │   │ bins correlated by eras and DM-bins) , 'syst_$ERA_up'/'syst_$ERA_down' (syst. uncertainty    │
# │   │ for low pT bins uncorrelated by eras $ERA=2016_preVFP,2016_postVFP,2017,2018) ,              │
# │   │ 'syst_$ERA_dm$DM_up'/'syst_$ERA_$DM_down' (syst. uncertainty for low pT bins uncorrelated by │
# │   │ eras and DM $ERA=2016_preVFP,2016_postVFP,2017,2018) $DM=0,1,10,11 ,                         │
# │   │ Values: down, stat1_dm0_down, stat1_dm0_up, stat1_dm10_down, stat1_dm10_up, stat1_dm11_down, │
# │   │ stat1_dm11_up, stat1_dm1_down, stat1_dm1_up, stat2_dm0_down, stat2_dm0_up, stat2_dm10_down,  │
# │   │ stat2_dm10_up, stat2_dm11_down, stat2_dm11_up, stat2_dm1_down, stat2_dm1_up,                 │
# │   │ stat_highpT_bin1_down, stat_highpT_bin1_up, stat_highpT_bin2_down, stat_highpT_bin2_up,      │
# │   │ stat_ptbin1_down, stat_ptbin1_up, stat_ptbin2_down, stat_ptbin2_up, stat_ptbin3_down,        │
# │   │ stat_ptbin3_up, stat_ptbin4_down, stat_ptbin4_up, stat_ptbin5_down, stat_ptbin5_up,          │
# │   │ stat_ptbin6_down, stat_ptbin6_up, stat_ptbin7_down, stat_ptbin7_up, stat_ptbin8_down,        │
# │   │ stat_ptbin8_up, stat_ptbin9_down, stat_ptbin9_up, syst_2018_dm0_down, syst_2018_dm0_up,      │
# │   │ syst_2018_dm10_down, syst_2018_dm10_up, syst_2018_dm11_down, syst_2018_dm11_up,              │
# │   │ syst_2018_dm1_down, syst_2018_dm1_up, syst_2018_down, syst_2018_up, syst_alleras_down,       │
# │   │ syst_alleras_up, syst_highpT_down, syst_highpT_extrap_down, syst_highpT_extrap_up,           │
# │   │ syst_highpT_up, up                                                                           │
# │   │ has default                                                                                  │
# │   ╰──────────────────────────────────────────────────────────────────────────────────────────────╯

def listAllTauVSJetSystematics_pt(year, runPeriod=None, addCombined=True):
    # year = str(year)
    # era = year if runPeriod is None else year+"_"+runPeriod
    # Statistical uncertainties on each of the binned SF values. There are 2 uncertainties per era, 1 for each pT measurment region (=2*4=8 total). These systematics are named like "stat_highpT_bin$i_{up,down}" where $i=1,2.
    # The systematic uncertainties that are fully correlated across bins and eras. This uncertainty is named "syst_highpT_{up,down}" in the jsons.
    # A systematic uncertainty to cover the extrapolation of the SF to higher pT regions that is fully correlated across bins and eras. This is needed since most of the W*->taunu events in the pT>200 GeV bin are < 300 GeV. This uncertainty is named "syst_highpT_extrap_{up,down}" in the jsons.
    return  (
        ["nom"] + (["up", "down"] if addCombined else []) +
        [f"stat_highpT_bin{i}_{direction}" for i in (1, 2) for direction in ("up", "down")] + 
        ["syst_highpT_up", "syst_highpT_down"] + 
        ["syst_highpT_extrap_up", "syst_highpT_extrap_down"]
    )

def listAllTauVSJetSystematics_dm(year, runPeriod, addCombined=True):
    year = str(year)
    era = year+"_"+runPeriod if runPeriod else year
    # Statistical uncertainties on the linear fit parameters. There are two uncertainties per DM and era (2x4x4=16 total), these systematics are named like "stat$i_dm$DM_{up,down}" where $i=1,2 and $DM=0,1,10,11.
    # A systematic uncertainty fully correlated between eras and DMs. This is named "syst_alleras_{up,down}" in the jsons.
    # Systematic uncertainty correlated between DMs but uncorrelated by eras (4 eras = 4 systematics total). These are named like "syst_$ERA_{up,down}" in the jsons where $ERA=2016_preVFP, 2016_postVFP, 2017, or 2018
    # Systematic uncertainty uncorrelated between DMs and eras (4 eras x 4 DMs = 16 systematics total). These are named like "syst_$ERA_dm$DM_{up,down}" in the jsons
    return (
        ["nom"] + (["up", "down"] if addCombined else []) +
        [f"stat{i}_dm{dm}_{direction}" for i in (1,2) for dm in (0,1,10,11) for direction in ("up","down")] +
        ["syst_alleras_up", "syst_alleras_down"] +
        [f"syst_{era}_up", f"syst_{era}_down"] +
        [f"syst_{era}_dm{dm}_{direction}" for dm in (0,1,10,11) for direction in ("up","down")]
    )


class TauSFRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year");  year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod", None)
        
        self.vsjet_wps = kwargs.pop("vsjet_wps", ["Medium", "Tight"])
        self.vse_wps = kwargs.pop("vse_wps", ["VVLoose", "Tight"])
        self.vsmu_wps = kwargs.pop("vsmu_wps", ["VLoose", "Tight"])
        self.vsjet_vse_wp = kwargs.pop("vsjet_vse_wp", "VVLoose")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        if self.runPeriod:
            prefix += self.runPeriod

        self.corrKey = prefix+year
        corrCfg = envyaml.EnvYAML('%s/src/Corrections/TAU/python/tauCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

        try:
            self.tauIdAlgo = kwargs.pop("tauId_algo")
            self.tauIdAlgo = self.tauIdAlgo[2:]
        except KeyError:
            self.tauIdAlgo = corrCfg[self.corrKey]["corrName"]
            print("** WARNING: tau ID algorithm specified for corrections! "
                  "Defaulting to correction name from Yaml: "+self.tauIdAlgo)
         

        self.vsjet_pt_binned_uncertainties = kwargs.pop("vsjet_pt_binned_uncertainties", ["nom", "up", "down"])
        if self.vsjet_pt_binned_uncertainties == "all":
            self.vsjet_pt_binned_uncertainties = listAllTauVSJetSystematics_pt(self.year, self.runPeriod)
        self.vsjet_dm_binned_uncertainties = kwargs.pop("vsjet_dm_binned_uncertainties", ["nom", "up", "down"])
        if self.vsjet_dm_binned_uncertainties == "all":
            self.vsjet_dm_binned_uncertainties = listAllTauVSJetSystematics_dm(self.year, self.runPeriod)
        
        if self.isMC:
            if not isUL and self.year <= 2018:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_TauSF"):
                os.environ["_TauSF"] = "_TauSF"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")
                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vsjet = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], self.tauIdAlgo+"VSjet"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vse = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], self.tauIdAlgo+"VSe"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vsmu = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], self.tauIdAlgo+"VSmu"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tes = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrNameTES"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_deeptau_vsjet_sf(
                            Vfloat pt, Vint dm, Vint genmatch,
                            std::string wp, std::string wp_vse, std::string syst, std::string flag) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (dm[i] > 2 && dm[i] < 10) {
                                sf.push_back(1.); // SFs only provided for DM 0,1,10,11
                            }
                            else 
                                sf.push_back(corr_vsjet.eval({pt[i], dm[i], genmatch[i], wp, wp_vse, syst, flag}));
                        }
                        return sf;
                    }

                    ROOT::RVec<double> get_deeptau_vse_sf(
                            Vfloat eta, Vint dm, Vint genmatch, std::string wp, 
                            std::string syst, std::string idV) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < eta.size(); i++) {
                            if (dm[i] >= 2 && dm[i] < 10) sf.push_back(1.);
                            else if (idV == "DeepTau2017v2p1") {
                                sf.push_back(corr_vse.eval({eta[i], genmatch[i], wp, syst}));
                            }
                            else { 
                                sf.push_back(corr_vse.eval({eta[i], dm[i], genmatch[i], wp, syst}));
                            }
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_deeptau_vsmu_sf(
                            Vfloat eta, Vint genmatch, std::string wp, std::string syst) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < eta.size(); i++) {
                            sf.push_back(corr_vsmu.eval({eta[i], genmatch[i], wp, syst}));
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_tes(
                                Vfloat pt, Vfloat eta, Vint dm, Vint genmatch, std::string idV, 
                                std::string wp, std::string wp_vse, std::string syst) {
                            ROOT::RVec<double> tes_factor;
                            for (size_t i = 0; i < eta.size(); i++) {
                                if (dm[i] == 5 || dm[i] == 6) tes_factor.push_back(1.);
                                else if (idV == "DeepTau2017v2p1") {
                                    tes_factor.push_back(corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, syst}));
                                }
                                else {
                                    tes_factor.push_back(corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, wp, wp_vse, syst}));
                                }
                            }
                            return tes_factor;
                        }
                """)

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        if self.tauIdAlgo == "DeepTau2017v2p1": # pt binned SFs are available only for DeepTau2017v2p1
            for syst in self.vsjet_pt_binned_uncertainties:
                if syst == "nom":
                    syst = ""
                    syst_name = ""
                else:
                    syst_name = "_"+syst
                
                for wp in self.vsjet_wps:
                    # pt binned SFs are available only for DeepTau2017v2p1
                
                    df = df.Define("Tau_sf%sVSjet_pt_binned_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name),
                                    'get_deeptau_vsjet_sf(Tau_pt, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "pt")' %
                                    (wp, self.vsjet_vse_wp, syst))

                    branches.append("Tau_sf%sVSjet_pt_binned_%s%s" %
                                    (self.tauIdAlgo, wp, syst_name))
        for syst in self.vsjet_dm_binned_uncertainties:
            if syst == "nom":
                syst = ""
                syst_name = ""
            else:
                syst_name = "_"+syst

            for wp in self.vsjet_wps:
                df = df.Define("Tau_sf%sVSjet_dm_binned_%s%s" % 
                                (self.tauIdAlgo, wp, syst_name),
                                'get_deeptau_vsjet_sf(Tau_pt, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "dm")' % 
                                (wp, self.vsjet_vse_wp, syst))

                branches.append("Tau_sf%sVSjet_dm_binned_%s%s" % 
                                (self.tauIdAlgo, wp, syst_name))

                

        for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
            for wp in self.vse_wps:
                df = df.Define("Tau_sf%sVSe_%s%s" % 
                                (self.tauIdAlgo, wp, syst_name),
                                'get_deeptau_vse_sf(Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s")' % 
                                (wp, syst, self.tauIdAlgo))

                branches.append("Tau_sf%sVSe_%s%s" % 
                                (self.tauIdAlgo, wp, syst_name))

            for wp in self.vsmu_wps:
                df = df.Define("Tau_sf%sVSmu_%s%s" % 
                                (self.tauIdAlgo, wp, syst_name),
                                'get_deeptau_vsmu_sf(Tau_eta, Tau_genPartFlav, "%s", "%s")' % 
                                (wp, syst))

                branches.append("Tau_sf%sVSmu_%s%s" % 
                                (self.tauIdAlgo, wp, syst_name))

            # tes factor is WP dependent for DeepTau2018v2p5
            if self.year >= 2022:
                for wp in self.vsjet_wps:
                    df = df.Define("tes_factor_%s%s" % (wp, syst_name),
                                    'get_tes(Tau_pt, Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "%s")' % 
                                    (self.tauIdAlgo, wp, self.vsjet_vse_wp, syst))
                    
                    df = df.Define("Tau_pt_corr_%s%s" % (wp, syst_name),
                                    "Tau_pt * tes_factor_%s%s" % (wp, syst_name))
                    
                    df = df.Define("Tau_mass_corr_%s%s" % (wp, syst_name),
                                    "Tau_mass * tes_factor_%s%s"  % (wp, syst_name))
                
                    branches.append("tes_factor_%s%s" % (wp, syst_name))
                    branches.append("Tau_pt_corr_%s%s" % (wp, syst_name))
                    branches.append("Tau_mass_corr_%s%s" % (wp, syst_name))
            
            # tes factor is NOT WP dependent for DeepTau2017v2p1
            elif self.year <= 2018:
                df = df.Define("tes_factor%s" % syst_name,
                                'get_tes(Tau_pt, Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "dummy", "dummy", "%s")' % 
                                (self.tauIdAlgo, syst))
                
                df = df.Define("Tau_pt_corr%s" % syst_name,
                                "Tau_pt * tes_factor%s" % syst_name)
                
                df = df.Define("Tau_mass_corr%s" % syst_name,
                                "Tau_mass * tes_factor%s"  % syst_name)
            
                branches.append("tes_factor%s" % syst_name)
                branches.append("Tau_pt_corr%s" % syst_name)
                branches.append("Tau_mass_corr%s" % syst_name)

        return df, branches


def tauSFRDF(**kwargs):
    """
    Module to obtain DeepTau2017v2p1/DeepTau2018v2p5 SFs and TES with their up/down uncertainties.
    Computes both pt and dm-binned SFs (applied in dauIdIso.py, choosing which one based on tau pt)

    :param vsjet_wps: DeepTau2017v2p1VSjet/DeepTau2018v2p5VSjet WPs to consider. Default: `[Medium, Tight]`
    For DeepTau2017v2p1VSjet available SFs are Loose, Medium, Tight, VTight
    :type vsjet_wps: list of str

    :param vse_wps: DeepTau2017v2p1VSe/DeepTau2018v2p5VSe WPs to consider.  Default: `[VVLoose, Tight]`
    :type vse_wps: list of str

    :param vsmu_wps: DeepTau2017v2p1VSmu/DeepTau2018v2p5VSmu WPs to consider. Default: `[VLoose, Tight]`
    :type vsmu_wps: list of str

    :param vsjet_vse_wp: DeepTau2017v2p1VSeWP/DeepTau2018v2p5VSeWP used to compute the DeepTau2017v2p1VSjet/DeepTau2018v2p5VSjet scale factor.
        Default: `VVLoose`
        For DeepTau2017v2p1VSe available SFs are Tight, VVLoose 
    :type vsjet_vse_wp: str

    :param vsjet_uncertainties: Uncertainties to be computed for DeepTau2017v2p1VSjet scale factor. 
        Possible values : down, stat1_dm0_down, stat1_dm0_up, stat1_dm10_down, stat1_dm10_up, stat1_dm11_down, 
          stat1_dm11_up, stat1_dm1_down, stat1_dm1_up, stat2_dm0_down, stat2_dm0_up, stat2_dm10_down,  
          stat2_dm10_up, stat2_dm11_down, stat2_dm11_up, stat2_dm1_down, stat2_dm1_up,                 
          stat_highpT_bin1_down, stat_highpT_bin1_up, stat_highpT_bin2_down, stat_highpT_bin2_up,      
          stat_ptbin1_down, stat_ptbin1_up, stat_ptbin2_down, stat_ptbin2_up, stat_ptbin3_down,        
          stat_ptbin3_up, stat_ptbin4_down, stat_ptbin4_up, stat_ptbin5_down, stat_ptbin5_up,          
          stat_ptbin6_down, stat_ptbin6_up, stat_ptbin7_down, stat_ptbin7_up, stat_ptbin8_down,        
          stat_ptbin8_up, stat_ptbin9_down, stat_ptbin9_up, syst_2018_dm0_down, syst_2018_dm0_up,      
          syst_2018_dm10_down, syst_2018_dm10_up, syst_2018_dm11_down, syst_2018_dm11_up,              
          syst_2018_dm1_down, syst_2018_dm1_up, syst_2018_down, syst_2018_up, syst_alleras_down,       
          syst_alleras_up, syst_highpT_down, syst_highpT_extrap_down, syst_highpT_extrap_up,           
          syst_highpT_up, up
        Default: [nom, up, down]
    :type vsjet_uncertainties: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: tauSFRDF
            path: Corrections.TAU.tauCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isUL: self.dataset.has_tag('ul')
                runPeriod: self.dataset.runPeriod
                tauId_algo: self.config.tauId_algo
                vsjet_wps: [Medium, Tight]
                vse_wps: [VVLoose, Tight]
                vsmu_wps: [VLoose, Tight]

    """

    return lambda: TauSFRDFProducer(**kwargs)
